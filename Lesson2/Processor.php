<?php

use \myNs2\User;

class Processor extends Product
{
    //private string $name;
    //private float $price;
    //private User $owner;
    private float $frequency;

    public function __construct(
        string $name,
        float $price,
        User $owner,
        float $frequency
    )
    {
        parent::__construct($name, $price, $owner);

        $this->frequency = $frequency;

        parent::registerProduct($this);
    }
}