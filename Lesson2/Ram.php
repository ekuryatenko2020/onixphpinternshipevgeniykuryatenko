<?php

use \myNs2\User;

class Ram extends Product
{
    //private string $name;
    //private float $price;
    //private User $owner;
    private string $type;
    private int $memory;

    public function __construct(
        string $name,
        float $price,
        User $owner,
        string $type,
        int $memory
    )
    {
        parent::__construct($name, $price, $owner);

        $this->type = $type;
        $this->memory = $memory;

        parent::registerProduct($this);
    }
}