<?php

trait PrintInfoTrait {
    public function printProduct() {
        return get_called_class() . ": " . $this->name
            . "; price: " . $this->price
            . "; owner: " . $this->owner->getName();
    }
}
