<?php

//require_once "lesson2.php";
use \myNs2\User;

abstract class Product
{
    private string $name;
    private float $price;
    private User $owner;
    private static array $products = [];

    public function __construct($name, $price, $owner)
    {
        $this->name = $name;
        $this->price = $price;
        $this->owner = $owner;
    }

    public static function registerProduct($product)
    {
        if (!self::isRegisteredProduct($product)) {
            array_push(self::$products, $product);
        }
    }

    private static function isRegisteredProduct($product)
    {
        return in_array($product, self::$products, true);
    }

    public function __toString(): string
    {
        return get_called_class() . ": " . $this->name
            . "; price: " . $this->price
            . "; owner: " . $this->owner->getName();
    }

    public static function getIterator()
    {
        return new class (self::$products) implements Iterator {
            private int $position = 0;
            private array $array;

            public function __construct($products)
            {
                $this->array = $products;
            }

            public function rewind()
            {
                $this->position = 0;
            }

            public function current()
            {
                return $this->array[$this->position];
            }

            public function key()
            {
                return $this->position;
            }

            public function next()
            {
                ++$this->position;
            }

            public function valid()
            {
                return isset($this->array[$this->position]);
            }
        };
    }
}