<?php

namespace {
    require_once "lesson2.php";
    require_once "Product.php";
    require_once "Processor.php";
    require_once "Ram.php";

    use myNs2\User;

    $u1 = new User("U1", 100);
    $u2 = new User("U2", 100);

//    echo $u1 . " \n";
//    echo $u2 . " \n";
//
//    echo $u2->giveMoney($u1, 30) . " \n";
//
//    echo $u1 . " \n";
//    echo $u2 . " \n";

    $p1 = new Processor(
        "AMD",
        100,
        $u1,
        1700
    );

    $p2 = new Processor(
        "Intel",
        200,
        $u1,
        2700
    );

    $p3 = new Processor(
        "AMD",
        100,
        $u2,
        1700
    );

    $m1 = new Ram(
        "Goodram",
        20,
        $u1,
        "DDR3",
        4
    );

    $m2 = new Ram(
        "Hynix",
        80,
        $u1,
        "DDR3",
        16
    );

    $m3 = new Ram(
        "Hynix",
        80,
        $u1,
        "DDR3",
        16
    );

    foreach (Product::getIterator() as $product) {
        echo $product . "\n";
    }
}

namespace myNs1 {
    class User
    {
        private string $name;
        private int $balance;

        public function __construct(
            string $name,
            int $initialBalance
        )
        {
            $this->name = $name;
            $this->balance = $initialBalance;
        }

        public function giveMoney(User $gettingUser, int $sum)
        {
            $amount = ($this->getBalance() >= $sum) ?
                $sum :
                $this->getBalance();

            $gettingUser->addToBalance($amount);
            $this->addToBalance(-1 * $amount);

            return "Пользователь {$this->getName()} "
                . "перечислил {$amount} "
                . "пользователю {$gettingUser->getName()}";
        }

        private function addToBalance(int $sum)
        {
            $this->balance += $sum;
        }

        public function getBalance(): int
        {
            return $this->balance;
        }

        public function getName(): string
        {
            return $this->name;
        }

        public function __toString(): string
        {
            return "У пользователя {$this->getName()} "
                . "сейчас на счету {$this->getBalance()}";
        }
    }
}