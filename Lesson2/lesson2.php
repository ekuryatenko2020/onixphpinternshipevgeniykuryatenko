<?php

namespace myNs2;

class User
{
    private string $name;
    private int $balance;

    public function __construct(
        string $name,
        int $initialBalance
    )
    {
        $this->name = $name;
        $this->balance = $initialBalance;
    }

    public function giveMoney(User $gettingUser, int $sum)
    {
        $amount = ($this->getBalance() >= $sum) ?
            $sum :
            $this->getBalance();

        $gettingUser->addToBalance($amount);
        $this->addToBalance(-1 * $amount);

        return "Пользователь {$this->getName()} "
            . "перечислил {$amount} "
            . "пользователю {$gettingUser->getName()}";
    }

    private function addToBalance(int $sum)
    {
        $this->balance += $sum;
    }

    public function getBalance(): int
    {
        return $this->balance;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return "У пользователя {$this->getName()} "
            . "сейчас на счету {$this->getBalance()}";
    }
}



