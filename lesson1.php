<?php
class User
{
    private string $name;
    private int $balance;
   
    public function printStatus()
	{
        echo "У пользователя {$this->getName()} "
            ."сейчас на счету {$this->getBalance()}"
            ."\n";
    }
    
    public function giveMoney(?User $gettingUser, int $sum) 
	{
		if ($gettingUser == null) {
			 $this->balance = $sum;
		} else {
            $amount = ($this->getBalance() >= $sum) ? 
                $sum : 
                $this->getBalance();
		    
    	    $gettingUser->giveMoney(
    	        null, 
    	        ($gettingUser->getBalance() + $amount)
    	    );
    	    
    	    $this->giveMoney(
    	        null, 
    	        ($this->getBalance() - $amount)
    	    );
    	    
    	    echo "Пользователь {$this->getName()} "
         	    ."перечислил {$amount} "
				."пользователю {$gettingUser->getName()} "
				."\n";
		}
    }
	
	public function setName(string $n)
	{
        $this->name = $n;
    }
	
	public function getBalance(): int
	{
        return $this->balance;
    }	
	
	public function getName(): string
	{
        return $this->name;
    }
} 

$u1 = new User();
$u1->setName("U1");
$u1->giveMoney(null, 100);

$u2 = new User();
$u2->setName("U2");
$u2->giveMoney(null, 200);

$u1->printStatus();
$u2->printStatus();

$u2->giveMoney($u1, 30);

$u1->printStatus();
$u2->printStatus();
?>