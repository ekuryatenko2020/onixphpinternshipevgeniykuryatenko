<?php

namespace myNs3;

class User
{
    private string $name;
    private int $balance;

    public function __construct(
        string $name,
        int $initialBalance
    )
    {
        $this->name = $name;
        $this->balance = $initialBalance;
    }

    public function giveMoney(User $gettingUser, int $sum)
    {
        $amount = ($this->getBalance() >= $sum) ?
            $sum :
            $this->getBalance();

        $gettingUser->addToBalance($amount);
        $this->addToBalance(-1 * $amount);

        return "Пользователь {$this->getName()} "
            . "перечислил {$amount} "
            . "пользователю {$gettingUser->getName()}";
    }

    private function addToBalance(int $sum)
    {
        $this->balance += $sum;
    }

    public function getBalance(): int
    {
        return $this->balance;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return "У пользователя {$this->getName()} "
            . "сейчас на счету {$this->getBalance()}";
    }

    public function listProducts(): array
    {
        $register = iterator_to_array(Product::getIterator());
        return array_filter($register, function ($item) {
            return ($item->getOwner() == $this);
        });
    }

    public function getListProductsArray(): array
    {
        $userProductsList = $this->listProducts();
        return array_map(function ($item) {
            return "Product: " . (new \ReflectionClass($item))->getShortName() . "; "
                . "name: " . $item->getName() . "; "
                . "price: " . $item->getPrice();
        }, $userProductsList);
    }

    public function sellProduct($buyer, $product): string
    {
        $thisUserProducts = $this->listProducts();
        if (in_array($product, $thisUserProducts, true)) {
            if ($buyer->getBalance() >= $product->getPrice()) {
                $buyer->giveMoney($this, $product->getPrice());
                $product->setOwner($buyer);
                return "Пользователь " . $this->getName() . " "
                    . "продал продукт " . $product->getName() . " "
                    . "($" . $product->getPrice() . ") "
                    . "пользователю " . $buyer->getName();
            } else {
                return "Пользователь " . $buyer->getName() . " "
                    . "не может перечислить $" . $product->getPrice() . " "
                    . "пользователю " . $this->getName() . " "
                    . "так как имеет только $" . $buyer->getBalance();
            }
        } else {
            return "Пользователь " . $this->getName() . " "
                . "не может продать продукт " . $product->getName() . " "
                . "($" . $product->getPrice() . ") "
                . "так как он принадлежит пользователю " . $product->getOwner()->getName();
        }
    }
}

abstract class Product
{
    private string $name;
    private float $price;
    private User $owner;
    private static array $products = [];

    // Constants for random products creation
    protected const MIN_PRICE = 1;
    protected const MAX_PRICE = 1000;
    protected const MIN_FREQ = 1.0;
    protected const MAX_FREQ = 3.0;
    protected const MIN_MEMORY = 1;
    protected const MAX_MEMORY = 128;

    public function __construct($name, $price, $owner)
    {
        $this->name = $name;
        $this->price = $price;
        $this->owner = $owner;
        $this->registerProduct();
    }

    private function registerProduct()
    {
        if (!self::isRegisteredProduct($this)) {
            array_push(self::$products, $this);
        }
    }

    private function isRegisteredProduct($product)
    {
        return in_array($product, self::$products, true);
    }

    public function __toString(): string
    {
        //Reflection faster than substr
        $shortName = "Product: " .
            (new \ReflectionClass($this))->getShortName() . "; ";
        return $shortName . " name: " . $this->name
            . "; price: " . $this->price
            . "; owner: " . $this->owner->getName();
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner($newOwner)
    {
        $this->owner = $newOwner;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    protected static function getRandomName(int $length): string
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, $length);
    }

    private static function createRandomAnonymClassProduct(User $user): Product
    {
        return new class($user) extends Product {
            public function __construct($user)
            {
                $name = self::getRandomName(10);
                $price = rand(self::MIN_PRICE, self::MAX_PRICE);
                parent::__construct($name, $price, $user);
            }
        };
    }

    private static function getRandomTypeFromProductList(): string
    {
        $productsList = Product::getIterator();
        $productClassesInList = [];
        foreach ($productsList as $product) {
            if (!in_array(get_class($product), $productClassesInList)) {
                array_push($productClassesInList, get_class($product));
            }
        }
        array_push($productClassesInList, "ANONYMOUS_CLASS");
        $randomKey = array_rand($productClassesInList);
        return $productClassesInList[$randomKey];
    }

    public static function createRandomProduct(User $user): Product
    {
        $randomType = self::getRandomTypeFromProductList();
        if (class_exists($randomType)) {
            return $randomType::createRandomProduct($user);
        } else {
            return self::createRandomAnonymClassProduct($user);
        }
    }

    public static function getIterator()
    {
        return new class (self::$products) implements \Iterator {
            private int $position = 0;
            private array $array;

            public function __construct($products)
            {
                $this->array = $products;
            }

            public function rewind()
            {
                $this->position = 0;
            }

            public function current()
            {
                return $this->array[$this->position];
            }

            public function key()
            {
                return $this->position;
            }

            public function next()
            {
                ++$this->position;
            }

            public function valid()
            {
                return isset($this->array[$this->position]);
            }
        };
    }
}

class Processor extends Product
{
    private float $frequency;

    public function __construct(
        string $name,
        float $price,
        User $owner,
        float $frequency
    )
    {
        parent::__construct($name, $price, $owner);
        $this->frequency = $frequency;
    }

    public static function createRandomProduct($user): Product
    {
        $productName = parent::getRandomName(5);
        $productPrice = rand(parent::MIN_PRICE, parent::MAX_PRICE);
        $productFrequency = rand(parent::MIN_FREQ, parent::MAX_FREQ);
        return new Processor(
            $productName,
            $productPrice,
            $user,
            $productFrequency
        );
    }
}

class Ram extends Product
{
    private string $type;
    private int $memory;

    public function __construct(
        string $name,
        float $price,
        User $owner,
        string $type,
        int $memory
    )
    {
        parent::__construct($name, $price, $owner);
        $this->type = $type;
        $this->memory = $memory;
    }

    public static function createRandomProduct($user): Product
    {
        $productName = parent::getRandomName(5);
        $productPrice = rand(parent::MIN_PRICE, parent::MAX_PRICE);
        $productPossibleTypesList = ["DDR3", "DDR4", "LPDDR4", "LPDDR5"];
        $productType = $productPossibleTypesList[array_rand($productPossibleTypesList)];
        $productMemory = rand(parent::MIN_MEMORY, parent::MAX_MEMORY);
        return new Ram(
            $productName,
            $productPrice,
            $user,
            $productType,
            $productMemory
        );
    }
}

/** Main code *****************************/
$u1 = new User("U1", 50);
$u2 = new User("U2", 100);

$p1 = new Processor(
    "AMD",
    100,
    $u1,
    1700
);

$p2 = new Processor(
    "Intel",
    200,
    $u1,
    2700
);

$p3 = new Processor(
    "AMD",
    100,
    $u2,
    1700
);

$m1 = new Ram(
    "Goodram",
    20,
    $u1,
    "DDR3",
    4
);

$m2 = new Ram(
    "Hynix",
    80,
    $u1,
    "DDR3",
    16
);

$m3 = new Ram(
    "Hynix",
    80,
    $u1,
    "DDR3",
    16
);

foreach ($u2->listProducts() as $product) {
    echo $product;
    echo PHP_EOL;
}

echo $u1->sellProduct($u2, $m1);
echo PHP_EOL;
echo $u1->sellProduct($u2, $m1);
echo PHP_EOL;
echo $u1->sellProduct($u2, $m2);
echo PHP_EOL;
echo $u1->sellProduct($u2, $m3);
echo PHP_EOL;

foreach ($u2->listProducts() as $p) {
    echo $p;
    echo PHP_EOL;
}

echo Product::createRandomProduct($u2);
echo PHP_EOL;
echo Ram::createRandomProduct($u2);
echo PHP_EOL;
echo Processor::createRandomProduct($u1);
