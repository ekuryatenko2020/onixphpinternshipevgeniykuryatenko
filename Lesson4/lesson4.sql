--Создать пользователя intern, разрешить пользователю вход по паролю
--Создать базу данных market
CREATE USER intern WITH PASSWORD '1';
CREATE DATABASE market WITH OWNER intern;
GRANT ALL PRIVILEGES ON DATABASE market TO intern;

--Создать таблицу users c полями реализованными для объекта из предыдущего задания (типы выбрать самостоятельно исходя из самого поля)
DROP TABLE IF EXISTS users;
CREATE TABLE users (
id int PRIMARY KEY,
name VARCHAR(50) NOT NULL,
balance MONEY DEFAULT 0
);

--Создать таблицу products c полями реализованными для объекта из предыдущего задания (типы выбрать самостоятельно исходя из самого поля)
--Реализовать связь между таблицами products и users по тому же принципу как и в предыдущем задании (у продукта есть хозяин)
--Продукт не может существовать без хозяина, при удалении хозяина продукт тоже должен удаляться
DROP TABLE IF EXISTS products_classes;
CREATE TABLE products_classes (
id int PRIMARY KEY,
name VARCHAR(50) UNIQUE NOT NULL
);

INSERT INTO products_classes (id, name) VALUES
(1, 'Processor'),
(2, 'Ram');

CREATE TABLE products (
id int PRIMARY KEY,
type VARCHAR(50) REFERENCES products_classes(name),
name VARCHAR(50) NOT NULL,
price MONEY DEFAULT 0,
owner_id int REFERENCES users(id) ON DELETE CASCADE,
processor_frequency REAL DEFAULT NULL,
ram_type VARCHAR(50) DEFAULT NULL,
ram_memory int DEFAULT NULL
);

--Заполнить базу несколькими пользователями и товарами для них
INSERT INTO users (id, name, balance)
VALUES (1, 'U1', 50);

INSERT INTO users (id, name, balance)
VALUES (2, 'U2', 100);

INSERT INTO products (
id,
type,
name,
price,
owner_id,
processor_frequency,
ram_type,
ram_memory
) VALUES
(1, 'Processor', 'AMD', 100, 1, 1.7, NULL, NULL),
(2, 'Ram', 'Goodram', 20, 2, NULL, 'DDR3', 16);

--Сделать выборку из двух таблиц (users left join products) но вывести только имя пользователя и имя продукта
SELECT
users.name,
products.type,
products.name
FROM users LEFT JOIN products ON (users.id = products.owner_id);

--Написать запрос изменения хозяина у продукта
INSERT INTO users (id, name, balance)
VALUES (3, 'U3', 200);

UPDATE products
SET owner_id =3
WHERE products.id = 1;

--Удалить одного пользователя
DELETE FROM users WHERE id = 1;

--Посчитать количество всех товаров у каждого пользователя одним запросом, вывести Имя пользователя и количество товаров
SELECT
users.name,
count(products.id) as products_qty
FROM users, products
WHERE (users.id = products.owner_id)
GROUP BY users.name;

--Добавить в таблицу users поле email, сделать его уникальным
ALTER TABLE users ADD COLUMN email TEXT UNIQUE CHECK (email LIKE '%@%.%');

--Сделать так чтобы айди пользователя начинались с 10000 при добавлении
CREATE SEQUENCE users_id_seq
INCREMENT 1
MINVALUE 10000
MAXVALUE 2147483647
START 10000
CACHE 1
OWNED BY users.id;

ALTER TABLE users ALTER COLUMN id
SET DEFAULT nextval('users_id_seq');

--Добавить в таблицу users поле birthday типа date, ограничить его чтобы возможно было добавить только дату в прошлом
ALTER TABLE users ADD COLUMN birthday DATE
CHECK (birthday < CURRENT_DATE)
DEFAULT '2000-01-01';

--Добавить поле age которое будет хранить количество лет пользователя
ALTER TABLE users ADD COLUMN age int
GENERATED ALWAYS AS (DATE_PART('year', date('now'))- DATE_PART('year', users.birthday::date) ) STORED;


--Добавить триггер который срабатывает при добавлении или обновлении записи в таблицу users и будет на 
--основании значения в поле birthday вычислять сколько полных лет пользователю и устанавливать значение в поле age
CREATE FUNCTION age_calc() RETURNS trigger AS $age_calc$
   BEGIN
       NEW.age := DATE_PART('year', date('now')) - DATE_PART('year', NEW.birthday::date);
       RETURN NEW;
   END;
$age_calc$ LANGUAGE plpgsql;

CREATE TRIGGER age_calc BEFORE INSERT OR UPDATE ON users
   FOR EACH ROW EXECUTE PROCEDURE age_calc();

--Написать транзакцию которая добавляет пользователя и несколько продуктов, если какой то из 
--запросов не выполняется, вся транзакция откатывается. 
BEGIN;

INSERT INTO users (name, balance, email, birthday)
VALUES ('U4', 100, 'a4@gmail.com', '1999-01-01');

INSERT INTO users (name, balance, email, birthday)
VALUES ('U5', 100, 'a5@gmail.com', '1989-01-01');

INSERT INTO products (
id,
type,
name,
price,
owner_id,
processor_frequency
) VALUES
(3, 'Processor', 'Intel', 400, 10000, 1.7);

INSERT INTO products (
id,
type,
name,
price,
owner_id,
ram_type,
ram_memory
) VALUES
(4, 'Ram', 'Goodram', 150, 10001, 'DDR3', 16);

COMMIT;
