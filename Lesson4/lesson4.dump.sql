--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: age_calc(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.age_calc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   BEGIN
       NEW.age := DATE_PART('year', date('now')) - DATE_PART('year', NEW.birthday::date);
       RETURN NEW;
   END;
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.products (
    id integer NOT NULL,
    type character varying(50),
    name character varying(50) NOT NULL,
    price money DEFAULT 0,
    owner_id integer,
    processor_frequency real,
    ram_type character varying(50) DEFAULT NULL::character varying,
    ram_memory integer
);


--
-- Name: products_classes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.products_classes (
    id integer NOT NULL,
    name character varying(50) NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    balance money DEFAULT 0,
    email text,
    birthday date DEFAULT '2000-01-01'::date,
    age integer GENERATED ALWAYS AS ((date_part('year'::text, '2020-09-16'::date) - date_part('year'::text, birthday))) STORED,
    CONSTRAINT users_birthday_check CHECK ((birthday < CURRENT_DATE)),
    CONSTRAINT users_email_check CHECK ((email ~~ '%@%.%'::text))
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 10000
    INCREMENT BY 1
    MINVALUE 10000
    MAXVALUE 2147483647
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.products (id, type, name, price, owner_id, processor_frequency, ram_type, ram_memory) FROM stdin;
2	Ram	Goodram	20,00р.	2	\N	DDR3	16
1	Processor	AMD	100,00р.	3	1.7	\N	\N
3	Processor	Intel	400,00р.	10000	1.7	\N	\N
4	Ram	Goodram	150,00р.	10001	\N	DDR3	16
\.


--
-- Data for Name: products_classes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.products_classes (id, name) FROM stdin;
1	Processor
2	Ram
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.users (id, name, balance, email, birthday) FROM stdin;
2	U2	100,00р.	\N	2000-01-01
3	U3	200,00р.	\N	2000-01-01
10000	U4	100,00р.	a4@gmail.com	1999-01-01
10001	U5	100,00р.	a5@gmail.com	1989-01-01
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.users_id_seq', 10001, true);


--
-- Name: products_classes products_classes_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products_classes
    ADD CONSTRAINT products_classes_name_key UNIQUE (name);


--
-- Name: products_classes products_classes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products_classes
    ADD CONSTRAINT products_classes_pkey PRIMARY KEY (id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users age_calc; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER age_calc BEFORE INSERT OR UPDATE ON public.users FOR EACH ROW EXECUTE FUNCTION public.age_calc();


--
-- Name: products products_owner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_owner_id_fkey FOREIGN KEY (owner_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: products products_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_type_fkey FOREIGN KEY (type) REFERENCES public.products_classes(name);


--
-- PostgreSQL database dump complete
--

